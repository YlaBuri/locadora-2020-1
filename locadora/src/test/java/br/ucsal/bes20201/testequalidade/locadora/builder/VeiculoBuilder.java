package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	public static final String PLACA_DEFAULT = "aaa1010";
	public static final Integer ANO_DEFAULT = 2010;
	public static final Modelo MODELO_DEAFAULT = new Modelo("Modelo Default");
	public static final Double VALOR_DIARIA_DEFAULT = 100.00;
	public static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa;
	private Integer ano;
	private Modelo modelo;
	private Double valorDiaria;
	private SituacaoVeiculoEnum situacao;

	private VeiculoBuilder() {
	}

	public static VeiculoBuilder veiculo() {
		return new VeiculoBuilder();
	}

	public static VeiculoBuilder veiculoDefault() {
		return VeiculoBuilder.veiculo()
				.comAno(ANO_DEFAULT)
				.comModelo(MODELO_DEAFAULT)
				.comPlaca(PLACA_DEFAULT)
				.comSituacao(SITUACAO_DEFAULT)
				.comValorDiaria(VALOR_DIARIA_DEFAULT);
	}
	
	public static VeiculoBuilder veiculo2019() {
		return VeiculoBuilder.veiculoDefault().comAno(2019);
	}

	public static VeiculoBuilder veiculo2012() {
		return VeiculoBuilder.veiculoDefault().comAno(2012);
	}
	
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder estaDisponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder estaLocado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder estaEmManutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder mas() {
		return VeiculoBuilder.veiculo().comAno(this.ano).comModelo(this.modelo).comPlaca(this.placa)
				.comSituacao(this.situacao).comValorDiaria(this.valorDiaria);
	}

	public Veiculo construir() {
		Veiculo v = new Veiculo();
		v.setAno(this.ano);
		v.setModelo(this.modelo);
		v.setPlaca(this.placa);
		v.setSituacao(this.situacao);
		v.setValorDiaria(this.valorDiaria);
		return v;
	}

}
