package br.ucsal.bes20201.testequalidade.locadora;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBOIntegradoTest {

	/*
	 * Testar o calculo do valor de loca��o por 3 dias para 2 veiculos com 1 ano de
	 * fabrica��o e 3 veiculos com 8 anos de fabrica��o.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		Veiculo v1 = VeiculoBuilder.veiculo2019().construir(); 
		Veiculo v2 = VeiculoBuilder.veiculo2019().construir();
		
		Veiculo v3 = VeiculoBuilder.veiculo2012().construir();
		Veiculo v4 = VeiculoBuilder.veiculo2012().construir();
		Veiculo v5 = VeiculoBuilder.veiculo2012().construir();
		
		List<Veiculo> veiculos = Arrays.asList(v1,v2,v3,v4,v5);
		
		VeiculoDAO.insert(v1);
		VeiculoDAO.insert(v2);
		VeiculoDAO.insert(v3);
		VeiculoDAO.insert(v4);
		VeiculoDAO.insert(v5);
		
		
		assertEquals(1410.0, LocacaoBO.calcularValorTotalLocacao(veiculos, 3));
		
	}

}
